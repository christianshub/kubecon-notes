# Been there, Done That: Tales of Burnout from the Open Source World
Speakers: Savitha Raghunathan, RedHat & Divya Mohan, SUSE

The speakers are focusing on the open source community and have been an active part. During the covid, by staying home it has been extremely difficult to maintain a healthy worklife balance.

## Why speak about burnout?

The work "burnout" is an abstract word. Could include stuff like poor worklife balance? Medical conditions? Emotional? 
Definition (WHO): "Chronic workplace stress that has not been successfully managed ..."

Being a part of the open source community people almost everyone takes part in a daytime job and in their freetime focus on the open source community. 

## How did the speakers get into a OS community?

Found it interesting they could take any role they'd like, developer, documentation creator etc in an OS project. But was also to gain a better CV for themselves. 

## How did they get better?

"It's not a sprint, it's a marathon".

Realizing it's okay to step down from the OS community as a maintainer until you are ready and want to contribute is okay. If you are part of a community who does not resepct that then it's not worth it to be a part of in the first place. 

Stepping down is not a step back.

## Need a "north star"

If there is no north star and if you, as a maintainer, have no clear goal for yourself and the project, then it can become chaotic and may end up being a project that dies. 

## Conclusion

There is no one solution, but a better inclusive and diverse understanding that it is a problem can help to address the problem in it's symptom-fase. 