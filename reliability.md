## What does Observability mean to you? 

Asked 100 people, 23 pct said "Graphs and traces", 29 pct said "Oops we didn't test that" and the rest and the top: "Information you didn't think you needed but could actually solve your problem"

- Gather requirements
- Build the thing
- Release it
- Experience incident (most of the talk revolves around this)
- Detect incident (most of the talk revolves around this)
- Restore service

## Trace, log, meassure

Insert your own error-codes in logs. Tracing extremely important to see how an end-user was using the system. 

## Experimentation (science and chaos)

- Observe baseline matrics (understand and document your system's normal state)
- Formulate hypotothesis (given a nominal staate)
- Understand blast radius (what parts of the system will be impacted by an experiment?) - make a chaos experiment, e.g. a CPU attack where you gradually increase CPU usage. 
- Set abort conditions (when to stop experimenting and revert back to a normal state?) 
- Analyze the results (what learnings can be derived from the experiment)
- Learn and improve (share the results and derive actions from the data)

## Four golden signals (signals and simulations)

The whole point of doing all this is to avoid the end-user to experience latency, error, traffic and saturation issues.

- `Latency`: The time it takes to service a request. Can be simulated by programmatically inject delays, change DNS network settings, change geo location.

- `Errors`: To simulate, terminate services, revoke access credentials, change system clock and timezones.

- `Traffic`: The demand that is placed on a system at any point). To simulate, create trafficspikes with tooling, change load-balacing to create hot spots, re-deploy on over-subscribed compute

- `Saturation`: The measure of system utilization and constraints. Simulate saturations can be done by altering scaling logic to deplay triggering. Fill up empty disk space and memory, run stress or consume.exe. 

