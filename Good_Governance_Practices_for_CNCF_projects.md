# Good Governance Practices for CNCF projects

## Governance is important

- To generate alignment
- Governence is about people. 
    - Defines how people collaborate and make decisions
- Important to set clear expectations up front
- Be explicit - documented governance increases project viability. 
- How are decisions made? Who makes those decisions? A clear path is important.
- If processes are not documented it introduces a lot of uncertainties 

## CNCF requirements, templates and how-to's

Templates can help with alignment. How-to's can be resources for governance and related topics. 

## Set expactations: Missions, values and Scope

Avoid misunderstandings. Think about it as a project charter.

## Mission statement

Purposes, advantages and key features. Often stated at the top of the README files. Helps new people get a grasp of the key features of the project.

## Project scope

Often overlooked in OS projects. Avoid misunderstandings and set expectations. E.g. a new user is using the project under the assumption the project can be taken into another direction than what was intended.

## Roles, responsibilies and proceudures

- Docs and procedures.. 

## Ladership

Contributers can become leaders. Should be documented.

## Ownership: Foundations or companies?

Important distinction. Governence is important where you have companies owning a project so contributors know exactly how they can raise in ranks to have more influence. Important so that companies wont just take the project into a certain direction they want. 

## Q&A's

Q: Example of where a project went wrong with no governance
A: Speaker was on a project with no governance and no direction. Ended up the speaker left it and VMWare stopped to use the project.
